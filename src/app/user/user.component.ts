import {Component, HostBinding, Input, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {EditComponent} from '../edit/edit.component';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})

export class UserComponent implements OnInit {
  @Input() user: any;

  constructor(
      private dialog: MatDialog
  ) {
  }

  ngOnInit() {
  }

  openEditModal() {
    const dialogRef = this.dialog.open(EditComponent, {
      height: '572px',
      width: '916px',
      data: {user: this.user},
    });

    dialogRef.afterClosed().subscribe(data => {
      if (data.save) {
        this.user = data.user;
      }
    });
  }
}
