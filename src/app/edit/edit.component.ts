import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material';
import {UsersService} from '../services/users.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {


  public editForm: FormGroup;
  public loading = false;

  constructor(
      private fb: FormBuilder,
      @Inject(MAT_DIALOG_DATA) public data: any,
      public dialogRef: MatDialogRef<EditComponent>,
      public snackBar: MatSnackBar,
      private userService: UsersService
  ) {
  }

  ngOnInit() {
    console.log(this.data);
    this.editForm = this.fb.group({
      fname: [this.data.user.fname],
      name: [this.data.user.name],
      mname: [this.data.user.mname],
      status: [this.data.user.status],
    });
  }

  closeAndSave($event: {}, id: any) {
    this.loading = true;
    const config: any = new MatSnackBarConfig();
    config.verticalPosition = 'top';
    config.horizontalPosition = 'right';
    config.duration = '3000';
    config.panelClass = ['success_message'];

    this.userService.updateUserDetails(this.editForm.value, this.data.user.id).subscribe((user: any) => {
      this.snackBar.open('Данные успешно сохранены !!!', '', config);
      this.dialogRef.close({user, save: true});
      this.loading = false;
    }, error => {
      this.loading = false;
      console.log(error);
    });

  }

  cancelEdit() {
    this.dialogRef.close();
  }
}
