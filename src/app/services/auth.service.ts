import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {getFromLocalStorage} from '../utils/local-storage';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
      private http: HttpClient
  ) {
  }

  login(user) {
    return this.http.post('https://frontend-test.cloud.technokratos.com/auth', user, { observe: 'response' });
  }

  getToken() {
    return getFromLocalStorage('BUMAGI_AUTH');
  }
}
