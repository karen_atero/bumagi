import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
      private http: HttpClient
  ) { }

  getUsersList(status) {
    return this.http.get(`https://frontend-test.cloud.technokratos.com/users?status=${status}`);
  }

  updateUserDetails(user: any, id) {
    return this.http.patch(`https://frontend-test.cloud.technokratos.com/users/${id}`, user);
  }
}
