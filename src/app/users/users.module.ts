import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UsersComponent} from './users.component';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {UserComponent} from '../user/user.component';
import {MomentModule} from 'ngx-moment';
import {MatDialogModule, MatIconModule, MatOptionModule, MatSelectModule} from '@angular/material';


const routes: Routes = [
  {
    path: '',
    component: UsersComponent
  }
];

@NgModule({
  declarations: [UsersComponent, UserComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    HttpClientModule,
    MomentModule,
  ]
})
export class UsersModule {
}
