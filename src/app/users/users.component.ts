import {Component, HostBinding, Input, OnInit} from '@angular/core';
import {UsersService} from '../services/users.service';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material';
import {Router} from '@angular/router';
import {animate, query, stagger, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  animations: [
    trigger('state', [
      state('hasUser', style({
        transform: 'translateX(0px)',
      })),
      state('noUser', style({
        transform: 'translateX(100%)',
      })),
      transition('* => *', animate('500ms'))
    ]),
    trigger('pageAnimations', [
      transition(':enter', [
        query('.app-users', [
          style({transform: 'translateY(-100%)'}),
          stagger(50, [
            animate('500ms', style({transform: 'translateY(0)'})),
          ]),

        ])
      ]),
      // transition(':leave', [
      //   query('.app-users', [
      //     style({transform: 'translateY(100%)'}),
      //     animate('0.5s ease-in-out', style({transform: 'translateY(-100%)'})),
      //   ])
      // ])
    ])
  ]
})

export class UsersComponent implements OnInit {

  @HostBinding('@state')
  @HostBinding('@pageAnimations')
  public animatePage = true;

  public users: any[] = [];
  public currentUsers = [];
  public filter = '';
  public loading = true;
  private reload;
  public state = 'noUser';

  constructor(
      private usersService: UsersService,
      public snackBar: MatSnackBar,
      private router: Router
  ) {
  }

  ngOnInit() {
    const config: any = new MatSnackBarConfig();
    config.verticalPosition = 'top';
    config.horizontalPosition = 'right';
    config.duration = '3000';
    this.state = 'noUser';
    // debugger;

    this.reload = setInterval(() => {
      this.usersService.getUsersList(this.filter).subscribe((users: any[]) => {
        if (Array.isArray(users)) {
          this.state = 'hasUsers';
          this.users = users.sort((first, last) => {
            return first.id - last.id;
          });
          this.loading = false;
        } else if (this.users.length === 0) {
          config.panelClass = ['server_error_message'];
          this.snackBar.open('Ошыбка - пробуем снова.', '', config);
        }
      }, error => {

        console.log(error.status);
        if (error.status === 403) {
          this.state = 'noUser';
          this.users = [];
          config.panelClass = ['server_error_message'];
          config.duration = '';
          clearInterval(this.reload);
          this.loading = false;

          const sb = this.snackBar.open(error.error.message, 'Войти', config);
          sb.onAction().subscribe(() => this.router.navigate(['']));
        } else if (this.users.length === 0) {
          this.snackBar.open('Ошыбка - пробуем снова.', '', config);
        }
      });
    }, 5000);
  }


  setFilter(s: string) {
    console.log(s);
    this.loading = true;
    this.state = 'noUser';
    this.users = [];

    this.filter = s;
  }

  animationDone() {
    this.currentUsers = this.users;
  }

  animationStarted() {
    if (this.users.length > 0) {
      this.currentUsers = this.users;
    }
  }
}
