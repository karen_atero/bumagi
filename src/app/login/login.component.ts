import {Component, HostBinding, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../services/auth.service';
import {setToLocalStorage} from '../utils/local-storage';
import {Router} from '@angular/router';
import {animate, query, stagger, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
    animations: [
        trigger('pageAnimations', [
            transition(':enter', [
                query('.app-login', [
                    style({transform: 'translateY(-100%)'}),
                  animate('0.5s', style({transform: 'translateY(0)'})),

                ])
            ]),
            transition(':leave', [
                query('.app-login', [
                    style({transform: 'translateY(0)'}),
                    animate('0.5s', style({transform: 'translateY(100%)'})),
                ])
            ])
        ])
    ]
})
export class LoginComponent implements OnInit {

  @HostBinding('@pageAnimations')
  public animatePage = true;
  passwordType = 'password';
  loginForm: FormGroup;

  constructor(
      private fb: FormBuilder,
      private auth: AuthService,
      private router: Router
  ) {
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      login: ['test@example.com', Validators.required],
      password: ['1q2w3e', Validators.required]
    });
  }

  login() {
    if (this.loginForm.valid) {
      this.auth.login(this.loginForm.value).subscribe((res: any) => {
        console.log(res.headers.get('authorization'));
        setToLocalStorage('BUMAGI_AUTH', res.headers.get('authorization'));
        this.router.navigate(['users']);
      });
    }
  }

  switchPasswordView() {
    this.passwordType === 'password' ? this.passwordType = 'text' : this.passwordType = 'password';
  }
}
